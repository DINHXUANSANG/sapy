import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name = "sapy-exp",
    version = "0.0.1",
    author = "sxuan29",
    author_email = "sxuan29@gmail.com",
    description = "a small example package",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://DINHXUANSANG@bitbucket.org/DINHXUANSANG/sapy.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)